# Test 1 - Cocktail Test (max. 2 hour) #

## Installation ##

composer install

php app/console doctrine:schema:create

php app/console doctrine:schema:update --force

## Result ##

** I didn't finish the test at all but the company liked the test so we arranged an interview :+1:**

I was wondering if they wanted me to use bootstrap to demonstrate i know about it but at the end i preferred to focus in the backend side. I used Symfony2 which gave me velocity to finish something in the time i had.

I think the main exercise here was to design the database. There is a ManyToMany relation where we have to add the field 'position' to be able manage/edit the order of the ingredients in a Cocktail. 

I think was a easy and in case i had more time i would create a good UI using bootstrap, jQuery and different UI javascript libraries.


## Description PHP Cocktail Test ##

### Introduction ###

● Write a PHP application as described below.

● Your code must work. Code design and style will also be evaluated.

● You may use the libraries or frameworks of your choice, as far as you specify those as

installation requirements.

● The response must be emailed back at most 3h​after receiving this document.

● Don’t worry if you don’t manage to do it all, this is just to get a feeling of what you are

capable of!

### Deliverables ###

● Source code of your application.

○ We are happy with code handed in in a zip file​, but we like it even better when

we get access to a privately shared git repository​(bitbucket provides free git

repositories). Please grant permission to HRexoclick.

○ We know how to use composer, npm, pip, etc... so don’t bother including ALL the

external libraries you use.

● Installation steps, including all installation requirements: libraries, frameworks, etc.

Unless very specific, you do not need to describe how to install Apache/Nginx,

PHP/Node.js/Python or MySQL/Mongodb etc...

● If applicable, SQL for the creation of the data model and some sample data to test your

application.

● If you use a framework, please try to write some code yourself and not only rely on its

code generation tools.
### Evaluation ###

Please, keep in mind we will take into account best practices.

We suggest you don't spend too much time on the frontend part; we are more interested in

backend :­)

### Requirements ###

The application will allow the users to prepare cocktails by adding and removing ingredients in

the correct order. An interface to change the order of the ingredients is expected.

● Ingredients

● Cocktails

○ An ingredient has a name and a raw cost price.

○ A cocktail has a name, a selling price and is made from several ingredients.

The selling price of a cocktail equals the total of all its ingredients plus an added 25% ​of the total

for the preparation.

● Examples:

○ The Caipirinha is made of the following ingredients in this order:

■ Cachaça: €2

■ Lime: €2

■ Refined sugar: €2

■ Total Price = €7.5

○ Long Island Iced Tea :

■ Vodka: €1

■ Tequila: €1

■ White Rum: €1

■ Triple Sec: €1

■ Gin: €2

■ Lemon juice: €1

■ Gomme Syrup: €0.50

■ Splash of Coke: €0.50

■ Total Price = €10