<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="order")
 */
class Order {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cocktail") 
     */
    protected $cocktail;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    function getId() {
        return $this->id;
    }

    function getCocktail() {
        return $this->cocktail;
    }

    function getPrice() {
        return $this->price;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCocktail($cocktail) {
        $this->cocktail = $cocktail;
    }

    function setPrice($price) {
        $this->price = $price;
    }

}
