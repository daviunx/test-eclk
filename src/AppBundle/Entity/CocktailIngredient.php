<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cocktail_ingredient")
 */
class CocktailIngredient {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ingredient")
     */
    protected $ingredient;

    /**
     * @ORM\ManyToOne(targetEntity="Cocktail", inversedBy="ingredients")
     */
    protected $cocktail;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $position;

    function getId() {
        return $this->id;
    }

    function getIngredient() {
        return $this->ingredient;
    }

    function getCocktail() {
        return $this->cocktail;
    }

    function getPosition() {
        return $this->position;
    }

    function setIngredient($ingredient) {
        $this->ingredient = $ingredient;
    }

    function setCocktail($cocktail) {
        $this->cocktail = $cocktail;
    }

    function setPosition($position) {
        $this->position = $position;
    }

}
