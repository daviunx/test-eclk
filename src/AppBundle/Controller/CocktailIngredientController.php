<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\CocktailIngredient;
use AppBundle\Form\CocktailIngredientType;

/**
 * CocktailIngredient controller.
 *
 * @Route("/cocktail-ingredient")
 */
class CocktailIngredientController extends Controller
{
    /**
     * Lists all CocktailIngredient entities.
     *
     * @Route("/", name="cocktailingredient_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cocktailIngredients = $em->getRepository('AppBundle:CocktailIngredient')->findAll();

        return $this->render('cocktailingredient/index.html.twig', array(
            'cocktailIngredients' => $cocktailIngredients,
        ));
    }

    /**
     * Creates a new CocktailIngredient entity.
     *
     * @Route("/new", name="cocktailingredient_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cocktailIngredient = new CocktailIngredient();
        $form = $this->createForm('AppBundle\Form\CocktailIngredientType', $cocktailIngredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cocktailIngredient);
            $em->flush();

            return $this->redirectToRoute('cocktailingredient_show', array('id' => $cocktailIngredient->getId()));
        }

        return $this->render('cocktailingredient/new.html.twig', array(
            'cocktailIngredient' => $cocktailIngredient,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CocktailIngredient entity.
     *
     * @Route("/{id}", name="cocktailingredient_show")
     * @Method("GET")
     */
    public function showAction(CocktailIngredient $cocktailIngredient)
    {
        $deleteForm = $this->createDeleteForm($cocktailIngredient);

        return $this->render('cocktailingredient/show.html.twig', array(
            'cocktailIngredient' => $cocktailIngredient,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CocktailIngredient entity.
     *
     * @Route("/{id}/edit", name="cocktailingredient_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CocktailIngredient $cocktailIngredient)
    {
        $deleteForm = $this->createDeleteForm($cocktailIngredient);
        $editForm = $this->createForm('AppBundle\Form\CocktailIngredientType', $cocktailIngredient);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cocktailIngredient);
            $em->flush();

            return $this->redirectToRoute('cocktailingredient_edit', array('id' => $cocktailIngredient->getId()));
        }

        return $this->render('cocktailingredient/edit.html.twig', array(
            'cocktailIngredient' => $cocktailIngredient,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CocktailIngredient entity.
     *
     * @Route("/{id}", name="cocktailingredient_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CocktailIngredient $cocktailIngredient)
    {
        $form = $this->createDeleteForm($cocktailIngredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cocktailIngredient);
            $em->flush();
        }

        return $this->redirectToRoute('cocktailingredient_index');
    }

    /**
     * Creates a form to delete a CocktailIngredient entity.
     *
     * @param CocktailIngredient $cocktailIngredient The CocktailIngredient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CocktailIngredient $cocktailIngredient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cocktailingredient_delete', array('id' => $cocktailIngredient->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
