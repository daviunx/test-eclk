<?php

namespace AppBundle\Calculator;

/**
 * Description of BasicCalculator
 *
 * @author dx
 */
class DefaultCalculator {

    private $comission;

    function __construct($comission) {
        $this->comission = $comission;
    }

    public function calculatePrice($cocktail) {
        return $cocktail->getPrice() * $this->comission / 100;
    }

}
